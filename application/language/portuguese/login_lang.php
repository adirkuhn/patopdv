<?php
$lang['login_login']='Iniciar Sessão';
$lang['login_username']='Usuário';
$lang['login_password']='Senha';
$lang['login_go']='Ir';
$lang['login_invalid_username_and_password']='Usuario/Senha inválidos';
$lang['login_welcome_message']='Bem vindo(a) Ao CS PDV. Para continuar, Inicie o sistema com seu nome de usuário e senha.';
$lang['login_version'] = 'Versão';
$lang['login_confirm_password'] = 'Confirmar Senha';
$lang['login_reset_password_for'] = 'Redefinir senha para';
$lang['login_reset_password'] = 'Perdi minha Senha';
$lang['login_reset_password_message'] = 'Foi feito um pedido para redefinir sua senha, por favor Clique no link abaixo para completar o processo de redefinição de Senha';
$lang['login_password_reset_has_been_sent'] = 'Redefinição de sua senha foi enviado para seu email. Por favor, veja seu email e clique no link que houver lá.';
$lang['login_password_has_been_reset'] = 'Senha redefinida com sucesso, por favor Clique aqui: ';
$lang['login_passwords_must_match_and_be_at_least_8_characters'] = 'Senhas deve ser iguais e ter no minimo 8 caracteres';
$lang['login_subscription_cancelled_within_30_days'] = 'sua incrição foi encerrada, por favor entre em contato com  contato@cloudsource.com.br para continuar com o serviço';
$lang['login_subscription_terminated'] = 'Seu periodo de avaliação terminou, entre em contato com contato@cloudsource.com.br para resolver';
$lang['login_application_mismatch'] = 'Existe um problema com a versão do sistema e a base de dados, por favor, actualice su base de dados antes de iniciar a sessão';
?>