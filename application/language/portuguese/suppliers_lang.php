<?php
$lang['suppliers_new']='Novo Fornecedor';
$lang['suppliers_supplier']='Fornecedor';
$lang['suppliers_update']='Atualizar Fornecedor';
$lang['suppliers_confirm_delete']='Tem certeza que deseja apagar os Fornecedores seleccionados?';
$lang['suppliers_none_selected']='Não foi selecionado nenhum Fornecedor para apagar';
$lang['suppliers_error_adding_updating'] = 'Erro ao adicionar/atualizar Fornecedor';
$lang['suppliers_successful_adding']='Fornecedor foi adicionando com sucesso';
$lang['suppliers_successful_updating']='Fornecedor foi atualizado com sucesso';
$lang['suppliers_successful_deleted']='Fornecedor deletado com sucesso';
$lang['suppliers_one_or_multiple']='Fornecedor(es)';
$lang['suppliers_cannot_be_deleted']='Não foi possível apagar os fornecedores selecionados. Existem vendas para um ou mais destes selecionados.';
$lang['suppliers_basic_information']='Informacões do Fornecedor';
$lang['suppliers_account_number']='Conta #';
$lang['suppliers_company_name']='Nome';
$lang['suppliers_company_name_required'] = 'Nome da Empresa é necessário';
?>