<?php
$lang['receivings_register']='Entrada de Produtos';
$lang['receivings_mode']='Modo de Entradas de Produtos';
$lang['receivings_receiving']='Receber Produtos';
$lang['receivings_return']='Voltar';
$lang['receivings_total']='Total';
$lang['receivings_cost']='Custo';
$lang['receivings_quantity']='Quantidade';
$lang['receivings_discount']='Desconto %';
$lang['receivings_edit']='Editar';
$lang['receivings_new_supplier'] = 'Novo Fornecedor';
$lang['receivings_supplier'] = 'Fornecedor';
$lang['receivings_select_supplier']='Selecionar Fornecedor (Opcional)';
$lang['receivings_start_typing_supplier_name']='Escrever o nome ...';
$lang['receivings_unable_to_add_item']='Não foi possível adicionar o produto a entrada de produtos';
$lang['receivings_error_editing_item']='Erro ao editar produto';
$lang['receivings_receipt']='Recibo de Entrada de Produto';
$lang['receivings_complete_receiving']='Terminar';
$lang['receivings_confirm_finish_receiving'] = 'Tem certeza que deseja processar esta entrada? Isto não pode ser desfeito.';
$lang['receivings_confirm_cancel_receiving'] = 'Tem certeza que deseja limpar esta entrada de produtos? Todos os produtos serão apagados.';
$lang['receivings_find_or_scan_item']='Encontrar/Escanear Produto';
$lang['receivings_find_or_scan_item_or_receipt']='Encontrar/Escanear Produto ou Entrada';
$lang['receivings_id']='ID da Entrada';
$lang['receivings_item_name'] = 'Nome do Produto';
$lang['receivings_transaction_failed'] = 'As transações de entrada falharam';
$lang['receivings_delete_successful'] = 'Recebimentos eliminados corretamente';

$lang['receivings_delete_unsuccessful'] = 'Não foi possível eliminar os recebimentos';

$lang['receivings_edit_receiving'] = 'Edicão de Recebimentos';

$lang['receivings_supplier'] = 'Fornecedor';

$lang['receivings_delete_entire_receiving'] = 'Apagar todos os produtos recebidos de';

$lang['receivings_successfully_updated'] = 'Recebimentos atualizados corretamente';

$lang['receivings_unsuccessfully_updated'] = 'Recebimentos, não foram atualizados';

$lang['receivings_undelete_entire_sale'] = 'Recuperar tudo o que foi recebido';

$lang['receivings_undelete_confirmation'] = 'Tem certeza que deseja recuperar os recebimentos?';

$lang['receivings_undelete_successful'] = 'Recebimentos recuperados com sucesso';


$lang['receivings_delete_unsuccessful'] = 'Recebimentos eliminados não foram recuperados';


$lang['receivings_cancel_receiving'] = 'Cancelar';
$lang['receivings_unable_to_add_supplier']='Não foi possível adicionar fornecedor';
?>