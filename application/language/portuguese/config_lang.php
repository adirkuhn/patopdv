<?php
$lang['config_info']='Salvar configuração';
$lang['config_company']='Nome da empresa';
$lang['config_address']='Endereço da empresa';
$lang['config_phone']='Telefone da empresa';
$lang['config_website']='Site';
$lang['config_fax']='Fax';
$lang['config_default_tax_rate']='% de Impostos Predeterminados';
$lang['config_default_tax_rate_1']='Taxa de Impostos 1';
$lang['config_default_tax_rate_2']='Taxa de Impostos 2';
$lang['config_company_required']='Nome da empresa requerido';
$lang['config_address_required']='Endereço da empresa requerido';
$lang['config_phone_required']='Telefone da empresa requerido';
$lang['config_default_tax_rate_required']='Taxa de impostos predeterminado requerido';
$lang['config_default_tax_rate_number']='Taxa de impostos predeterminado deve ser um número';
$lang['config_company_website_url']='Site não é um URL Valido (http://...)';
$lang['config_saved_successfully']='Configurações Salvas com sucesso';
$lang['config_saved_unsuccessfully']='Erro ao salvar Configurações';
$lang['config_return_policy_required']='Política de reembolso é requerida';
$lang['config_print_after_sale']='Imprimir recibo despois de uma venda?';
$lang['config_language'] = 'Idioma';
$lang['config_timezone'] = 'fuso horário';
$lang['config_currency_symbol'] = 'Símbolo da moeda';
$lang['config_backup_database'] = 'Backup da base de dados';
$lang['config_restore_database'] = 'Restaurar base de dados';
$lang['config_mailchimp_api_key'] = 'MailChimp Chave API'; 
$lang['config_number_of_items_per_page'] = 'Número de artigos por página';
$lang['config_date_format'] = 'Formato da data';
$lang['config_time_format'] = 'Formato da hora';
$lang['config_company_logo'] = 'Logotipo da empresa';
$lang['config_delete_logo'] = 'Apagar Logo?';
$lang['config_track_cash'] = 'Visualizar o movimento de caixa?';
$lang['config_optimize_database'] = 'Otimizar base de dados';
$lang['config_database_optimize_successfully'] = 'Base de dados otimizada com sucesso';
$lang['config_payment_types'] = 'Método de pagamentos';
$lang['config_speed_up_search_queries'] = 'Otimizar consultas de buscas?';
$lang['select_sql_file'] = 'selecione. sql';
$lang['restore_heading'] = 'Isto te permite restaurar a base de dados';

$lang['type_file'] = 'selecione. sql (tipo de arquivo)';

$lang['restore'] = 'restaurar';

$lang['required_sql_file'] = 'Arquivo selecionado inexistente';

$lang['restore_db_success'] = 'Base de dados restaurada com sucesso';

$lang['db_first_alert'] = 'Tem certeza que deseja restaurar a base de dados?';
$lang['db_second_alert'] = 'Os dados Serão perdidos, Continuar?';
$lang['password_error'] = 'Chave incorreta';
$lang['password_required'] = 'Campo da chave não pode estar em branco';
$lang['restore_database_title'] = 'Restaurar base de dados';
$lang['config_use_scale_barcode'] = 'Uso de códigos de barras de escala';
$lang['config_enable_credit_card_processing'] = 'Ativar Pagamento com cartão de crédito (Mercure)?';
$lang['config_environment'] = 'meio ambiente';
$lang['config_merchant_id'] = 'Merchant ID';
$lang['config_merchant_password'] = 'Chave do negocio';
$lang['config_sandbox'] = 'SandBox';
$lang['config_production'] = 'producão';
$lang['disable_confirmation_sale']='Desativar confirmação de venda?';
$lang['receive_stock_alert'] = 'Receber alertas de inventário baixo?';
$lang['stock_alert_email'] = 'E-mail para alertas';
$lang['sales_mercury_password_note'] = 'A Chave se encontra na conta Mercurio, Na area de configuração WebConfig >>> Key HostedCheckout';
$lang['config_default_payment_type'] = 'Método de pagamento padrão';
$lang['config_speed_up_note'] = 'Recomendado somente se existem mais de 10.000 artigos ou clientes';
$lang['config_hide_signature'] = 'Ocultar assinatura?';
$lang['config_automatically_email_receipt']='Recebimento automático de email';
$lang['config_barcode_price_include_tax']='Incluir impostos no código de barras?';
$lang['config_round_cash_on_sales'] = 'Arredondamento do valor das vendas';

?>