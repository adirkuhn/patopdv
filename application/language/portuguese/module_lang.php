<?php
$lang['module_home']='Início';

$lang['module_customers']='Clientes';
$lang['module_customers_desc']='Adicionar, Atualizar, apagar e buscar clientes';

$lang['module_suppliers']='Fornecedores';
$lang['module_suppliers_desc']='Adicionar, Atualizar, apagar e buscar Fornecedores';

$lang['module_employees']='Empregados';
$lang['module_employees_desc']='Adicionar, Atualizar, apagar e buscar empregados';

$lang['module_sales']='Vendas (F2)';
$lang['module_sales_desc']='Processar vendas e reembolsos';

$lang['module_reports']='Relatórios';
$lang['module_reports_desc']='Ver gerar relatórios';

$lang['module_items']='Estoque';
$lang['module_items_desc']='Adicionar, Atualizar, apagar e buscar produtos';

$lang['module_config']='LOja';
$lang['module_config_desc']='Configuração da loja';

$lang['module_receivings']='Pedidos';
$lang['module_receivings_desc']='Processar novos pedidos';

$lang['module_giftcards']='Presentes';
$lang['module_giftcards_desc']='Adicionar, Atualizar, apagar e buscar Cartões de Presentes';

$lang['module_item_kits']='Kits de Produtos';
$lang['module_item_kits_desc']='Adicionar, Atualizar, apagar e buscar Kits de Produtos';

$lang['module_action_add_update'] = 'Adicionar, atualizar';
$lang['module_action_delete'] = 'Apagar';
$lang['module_action_search_customers'] = 'Buscar clientes';
$lang['module_action_search_items'] = 'Elementos da busca';
$lang['module_action_search_item_kits'] = 'Itens para de busca de produto';
$lang['module_action_search_suppliers'] = 'Busca de Fornecedores';
$lang['module_see_cost_price'] = 'Ver preço de custo';
$lang['module_action_search_employees'] = 'Busca de empregados';
$lang['module_edit_sale_price'] = 'Editar preço de venda';
$lang['module_edit_sale_price'] = 'Editar Preço de Venda';
$lang['module_edit_sale'] = 'Editar Venda';
$lang['module_give_discount'] = 'dar desconto';
?>