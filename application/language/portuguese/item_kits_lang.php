<?php
$lang['item_kits_name'] = 'Nome do kit de produtos';
$lang['item_kits_description'] = 'Descrição do kit de produtos';
$lang['item_kits_no_item_kits_to_display'] = 'Não existe kit de produtos para mostrar';
$lang['item_kits_update'] = 'Atualizar kit de produtos';
$lang['item_kits_new'] = 'Novo kit de produtos';
$lang['item_kits_none_selected'] = "Não foi selecionado kit de produtos";
$lang['item_kits_info'] = 'Informação de kit de produtos';
$lang['item_kits_successful_adding'] = 'kit de produtos adicionado com sucesso';
$lang['item_kits_successful_updating'] = 'kit de produtos Atualizado com sucesso';
$lang['item_kits_error_adding_updating'] = 'Erro adicionar/atualizar o kit de produtos';
$lang['item_kits_successful_deleted'] = 'Apagado com sucesso';
$lang['item_kits_confirm_delete'] = 'Tem certeza que deseja apagar o kit de produtos selecionados?';
$lang['item_kits_one_or_multiple'] = 'kit de produtos(s)';
$lang['item_kits_cannot_be_deleted'] = 'Não foi possível apagar o kit de produtos';
$lang['item_kits_add_item'] = 'Adicionar item ao kit de produtos';
$lang['item_kits_items'] = 'Produtos';
$lang['item_kits_item'] = 'Produto';
$lang['item_kits_quantity'] = 'Quantidade';
?>